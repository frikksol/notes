#!/usr/bin/env bash

# Config parameters
editor=vim
reader=mdv

scriptpath="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"/

add ()
{
	category=$1

	cd ${scriptpath}

	echo Add note in category: ${category}
	read note

	$(mkdir -p ${scriptpath}/${category})
	$(touch ${scriptpath}${category}/$(echo ${note// /_}))
	$(echo "# ${note}" > ${scriptpath}${category}/$(echo ${note// /_}))


	${editor} ${scriptpath}${category}/$(echo ${note// /_})
	return
}

edit ()
{
	note=$1

	cd ${scriptpath}

	if [ -z ${note} ]
	then
		echo Enter name of note to edit \(fuzzy, but must be unique\)
		read note
	fi

	note_path=$(find -name "*$(echo ${note// /_})*")
	echo ${note_path}

	${editor} ${note_path}

	return
}

print ()
{
	note=$1

	cd ${scriptpath}

	if [ -z ${note} ]
	then
		echo Enter name of note to  print \(fuzzy, but must be unique\)
		read note
	fi

	note_path=$(find -name "*${note}*")
	echo ${note_path}

	if [ -z ${note_path} ]
	then
		echo Note not found. Exiting
		return
	fi

	${reader} ${note_path}

	return
}

clean_empty_folders ()
{
	cd ${scriptpath}

	rm -d ./*/ >/dev/null 2>&1
}

archive ()
{
	note=$1

	cd ${scriptpath}
	mkdir ./.archive >/dev/null 2>&1

	if [ -z ${note} ]
	then
		echo Enter name of note to archive \(fuzzy, but must be unique\)
		read note
	fi

	note_path=$(find -name "*${note}*")
	echo ${note}
	mv ${note_path} ./.archive

	clean_empty_folders

	return
}

delete ()
{
	note=$1

	cd ${scriptpath}

	if [ -z ${note} ]
	then
		echo Enter name of note to delete \(fuzzy\)
		read note
	fi

	rm -i ./*/*$(echo ${note// /_})*
	clean_empty_folders
}

list ()
{
	category=$1

	cd ${scriptpath}

	if [ -z ${category} ]
	then
		category=*
	elif [ ${category} == "all" ]
	then
		category=*
	fi

	output=$(ls -1Rc ${category}/)
	echo ${output} | sed -r 's/[^ ]+/\n&/g' | sed -r 's/(.*\/:)/\n&/g' | tr "_" " " | tr "/" " "
}

howto ()
{
	echo ""
	echo "--- notes ---"
	echo "simple single script note system, simply autocreating markdown notes"
	echo ""
	echo "--- options ---"
	echo "add category"
	echo "adds a new note in the chosen note category."
	echo ""
	echo "edit [note name]"
	echo "opens a note in the preferred text editor for editing, default: vim"
	echo ""
	echo "print [note name]"
	echo "outputs a note in the teminal using preferred reader, default: mdv"
	echo ""
	echo "archive [note name]"
	echo "archives a note. This implies moving it to the directory .archive"
	echo ""
	echo "delete [note name]"
	echo "deletes a note"
	echo ""
	echo "list category"
	echo "lists notes in chosen category. Without parameters it lists all."
	echo ""
	echo "list all"
	echo "lists all notes"
	echo ""
	echo "--- configuration ---"
	echo "at the top of the script variables for editor and reader can be set"
	echo ""
}

command=$1
first_arg=$2

if [ -z ${command} ]
then
	howto
	exit 1
fi

if [ ${command} == "add" ]
then
	add ${first_arg}
elif [ ${command} == "edit" ]
then
	edit ${first_arg}
elif [ ${command} == "print" ]
then
	print ${first_arg}
elif [ ${command} == "archive" ]
then
	archive ${first_arg}
elif [ ${command} == "delete" ]
then
	delete ${first_arg}
elif [ ${command} == "list" ]
then
	list ${first_arg}
else
	howto
fi


# NOTES
Simple single script note system, simply autocreating markdown notes.
This is no longer maintained as I found out Vimwiki is a thing

## Prerequesites
The only prerequesite for using this tool out of the box is [mdv](https://github.com/axiros/terminal_markdown_viewer). This is a python based terminal markdown viewer. If you want to avoid this, it is easy to change it in the configuration.

## Options
### add category
adds a new note in the chosen note category.

### edit [note name]
Opens a note in the preferred text editor for editing, default: vim

### print [note name]
Outputs a note in the teminal using preferred reader, default: mdv

### archive [note name]
Archives a note. This implies moving it to the directory .archive

### delete [note name]
Deletes a note

### list category
Lists notes in chosen category. Without parameters it lists all.

### list all
Lists all notes

## Configuration
At the top of the script, variables for editor and reader can be set


